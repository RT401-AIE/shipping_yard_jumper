﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyForce : MonoBehaviour
{
    Camera m_camera;
    public float forcetoApply = 500;

    public GameObject ExplosionPrefab;

    void Start()
    {
        m_camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);

            RaycastHit rayhit;

            if (Physics.Raycast(ray, out rayhit))
            {
                Rigidbody rb = rayhit.collider.GetComponent<Rigidbody>();
                Vector3 end = rayhit.transform.position;

                if(rb != null)
                {
                    Instantiate(ExplosionPrefab, end, Quaternion.identity);
                    rb.AddForce(ray.direction * forcetoApply);
                }
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);

            RaycastHit rayhit;

            if (Physics.Raycast(ray, out rayhit))
            {
                Rigidbody rb = rayhit.collider.GetComponent<Rigidbody>();

                if (rb != null)
                {
                    rb.AddForce(-ray.direction * forcetoApply);
                }
            }

        }
    }
}
