﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public string scene;
    public string Menu;

    private Canvas overviewCanvas;
    private Text RestartTest;
    private Text EscapeText;

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            ReloadScene();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReloadMenu();
        }
    }

    public void Start()
    {
        // sets the main menu scene
        Menu = "MainMenu";
    }

    public void ReloadScene()
    {
        // reloads the scene and UI scene while reseting the current time
        SceneManager.LoadScene(scene);
        SceneManager.LoadScene("UI", LoadSceneMode.Additive);
        ScoreBoard.instance.currentTime = 0;
    }

    public void ReloadMenu()
    {
        // loads the main menu scene
        SceneManager.LoadScene(Menu);
    }
}
