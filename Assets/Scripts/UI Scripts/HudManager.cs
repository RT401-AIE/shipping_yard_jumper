﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HudManager : MonoBehaviour
{
    public static HudManager instance;

    public TextMeshProUGUI timer;

    private void Awake()
    {
        instance = this;
    }
}
