﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionListUI : MonoBehaviour
{
    public LevelLoad prefab;

    // Start is called before the first frame update
    void Start()
    {
        var values = System.Enum.GetValues(typeof(LevelLoad.Difficulty));

        foreach (LevelLoad.Difficulty diff in values)
        {
            if (diff != LevelLoad.Difficulty.MainMenu)
            {
                // make this a child of ours on creation. 
                // Don't worry about specifying a position as the LayotGroup handles that

                LevelLoad ui = Instantiate(prefab, transform);
                ui.SetIndex((int)diff);
            }
        }
    }
}
