﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreBoard : MonoBehaviour
{
    // saved hiScores and shown scores below scene loading
    static public float[] HiScores = new float[6];
    static public float[] shownTime = new float[6];

    public float currentTime = 0;
    public int currentLevel;

    /// <summary>
    ///  create an instance of scoreBoard that will be used throught the game.
    /// </summary>
    public static ScoreBoard instance;


    private void Start()
    {
        // checks if a ScoreBoard already exists,
        if (instance != null)
        {
            // if it exists destroy newly created one
            Destroy(gameObject);
        }
        else
        {
            // if it doesnt exist make it not destroy on scene loading, and make instance = to this gameObject
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

    }

    public void Update()
    {
        // on update update timer and display on screen
        currentTime += Time.deltaTime;
        if(currentLevel != 0 && HudManager.instance != null)
            HudManager.instance.timer.SetText(TimeDisplay(currentTime));
    }

    public static string TimeDisplay(float time)
    {
        // dislpay time from a float to "time Seconds" 
        return time.ToString("0.00" + " Seconds");
    }

    public void FinishLevel()
    {
        // updates times when a trigger is entered
        if(shownTime[currentLevel] == 0)
        {
            HiScores[currentLevel] = currentTime;
            shownTime[currentLevel] = currentTime;
        }

        if (currentTime < shownTime[currentLevel])
        {
            HiScores[currentLevel] = currentTime;
            shownTime[currentLevel] = currentTime;
        }

        // reloads main menu
        SceneManager.LoadScene("MainMenu");
    }
}

