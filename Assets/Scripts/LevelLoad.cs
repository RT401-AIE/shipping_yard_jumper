﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelLoad : MonoBehaviour
{
    public string scene;
    public TextMeshProUGUI hiScore;
    public Text levelName;
    public int index;

    public void Start()
    {
        SetIndex(index);
    }

    public void LoadLevel()
    {
        // load the levels and resets the scoreboard currents
        SceneManager.LoadScene(scene);
        // loads the UI scene as an additive to overlay over the main scene
        SceneManager.LoadScene("UI", LoadSceneMode.Additive);
        ScoreBoard.instance.currentTime = 0;
        ScoreBoard.instance.currentLevel = index;
    }

    public enum Difficulty
    {
        MainMenu,
        ExtraEasy,
        Easy,
        Medium,
        Hard,
        Impossible
    }

    public void SetIndex(int i)
    {
        index = i;

        scene = ((Difficulty)i).ToString();

        // sets the levelname & hiScore if one exists
        if (levelName)
            levelName.text = ((Difficulty)i).ToString();

        if(hiScore)
            hiScore.text = ScoreBoard.TimeDisplay(ScoreBoard.HiScores[i]);
    }
}
