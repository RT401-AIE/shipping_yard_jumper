﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    public float timer = 0;

    void Update()
    {
        timer += Time.deltaTime;   

        if(timer > 1)
        {
            Destroy(gameObject);
        }
    }

}
