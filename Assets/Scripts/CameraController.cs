﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public int speed = 360;
    public float distance = 15;
    public float currentDistance;
    public float relaxSpeed = 10;
    public float heightOffset = 10;
    public Transform target;

    // Start is called before the first frame update
    void Start()
    {
        currentDistance = distance;
    }

    // Update is called once per frame
    void Update()
    {
        // right drag rotates the camera
        if(Input.GetMouseButton(1))
        {
            Vector3 angles = transform.eulerAngles;
            float dx = Input.GetAxis("Mouse Y");
            float dy= Input.GetAxis("Mouse X");
            
            // look up and down by rotating around X-axis
            angles.x = Mathf.Clamp(angles.x + dx * speed * Time.deltaTime, 0, 70);
            
            // spin the camera round 
            angles.y += dy * speed * Time.deltaTime;
            transform.eulerAngles = angles;
        }

        string[] layersToCollide = { "Default" };
        int layerMask = LayerMask.GetMask(layersToCollide);

        RaycastHit hit;
        if(Physics.Raycast(new Ray(GetTargetPosition(), -transform.forward), out hit, distance, layerMask))
        {
            // snap the camera right in to where the collision happened 
            currentDistance = hit.distance;
        }
        else
        {
            // relax the camera back to the desired distance
            currentDistance = Mathf.MoveTowards(currentDistance, distance, relaxSpeed);
        }
        
        // look at the target point
        transform.position = GetTargetPosition() - currentDistance * transform.forward;
    }

    Vector3 GetTargetPosition()
    {
        return target.position + heightOffset * Vector3.up;
    }
}
