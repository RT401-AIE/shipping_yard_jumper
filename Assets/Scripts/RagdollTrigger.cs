﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // sets ragdoll to active whenever the trigger is collided with
        Ragdoll ragdoll = other.gameObject.GetComponentInParent<Ragdoll>();
        if (ragdoll != null)
        {
            ragdoll.RagdollOn = true;
            CharacterMover cc = other.gameObject.GetComponentInParent<CharacterMover>();
            cc.isKinematic = true;
        }
    }
}
