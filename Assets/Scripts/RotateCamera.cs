﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{

    public float anglePerSecond = 10;
    public float anglesInDegrees;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        anglesInDegrees = Time.deltaTime * anglePerSecond;
        transform.RotateAround(new Vector3(0, 0, 0), Vector3.up, anglesInDegrees);
    }
}
