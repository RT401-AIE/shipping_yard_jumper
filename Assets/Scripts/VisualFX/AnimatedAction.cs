﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class AnimatedAction : ScriptableObject
{
    public string animTrigger;
    public VisualFXSystem.VisualFX beginFX; 
    public CharacterFX.BodyPart beginPart; 
    public VisualFXSystem.VisualFX activateFX; 
    public CharacterFX.BodyPart activatePart;

    public void Activate(CharacterFX character) 
    { 
        if (activateFX) 
            activateFX.Begin(character.GetBodyPart(activatePart)); 
        OnActivate(character); 
    }
    public abstract void OnActivate(CharacterFX character);
}
