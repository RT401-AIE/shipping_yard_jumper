﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour
{
    public float speed = 10;
    public float jumpVelocity = 10;
    public bool isGrounded;
    public Vector3 hitDirection;
    public bool isKinematic = false;

    CharacterController cc;
    Vector2 moveInput = new Vector2();
    public bool jumpInput;
    public Vector3 velocity = new Vector3();

    public Transform cam;
    public Animator animator;

    public SceneController SceneControl;
    
    void Update() 
    { 
        moveInput.x = Input.GetAxis("Horizontal"); 
        moveInput.y = Input.GetAxis("Vertical");
        jumpInput = Input.GetButton("Jump");

        animator.SetFloat("Forwards", Mathf.Abs(moveInput.y));
        animator.SetFloat("sense", moveInput.y > 0 ? 1 : -1);
        animator.SetBool("Jump", !isGrounded);
        animator.SetFloat("Turn", Input.GetAxis("Horizontal"));
    }
    
    // Start is called before the first frame update
    void Start()
    {
        // sets the variables
        animator = GetComponentInChildren<Animator>();
        cam = Camera.main.transform;
        cc = GetComponent<CharacterController>();
        SceneControl = GetComponent<SceneController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(isKinematic == false)
        { 
            // player movement using WASD or arrow keys
            Vector3 delta;

            // find the horizontal unit vector facing forward from the camera 
            Vector3 camForward = cam.forward;
            camForward.y = 0;
            camForward.Normalize();

            // use our camera's right vector, which is always horizontal
            Vector3 camRight = cam.right;

            delta = (moveInput.x * camRight + moveInput.y * camForward) * speed;

            if (isGrounded || moveInput.x != 0 || moveInput.y != 0)
            {
                velocity.x = delta.x;
                velocity.z = delta.z;
            }

            // check for jumping
            if (jumpInput && isGrounded)
                velocity.y = jumpVelocity;

            // check if we've hit ground from falling. If so, remove our velocity
            if (isGrounded && velocity.y < 0)
                velocity.y = 0;

            // apply gravity after zeroing velocity so we register as grounded still
            velocity += Physics.gravity * Time.fixedDeltaTime;

            if (!isGrounded)
                hitDirection = Vector3.zero;

            // slide objects off surfaces they're hanging on to
            if (moveInput.x == 0 && moveInput.y == 0)
            {
                Vector3 horizontalHitDirection = hitDirection;
                horizontalHitDirection.y = 0;
                float displacement = horizontalHitDirection.magnitude;

                if (displacement > 0)
                    velocity -= 0.2f * horizontalHitDirection / displacement;
            }

            transform.forward = camForward;

            cc.Move(velocity * Time.deltaTime);
            isGrounded = cc.isGrounded;
        }

        if (Input.GetKey(KeyCode.R))
        {
            SceneControl.ReloadScene();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            SceneControl.ReloadMenu();
        }
    }


    void OnControllerColliderHit(ControllerColliderHit hit) 
    { 
        hitDirection = hit.point - transform.position; 
    }
}
