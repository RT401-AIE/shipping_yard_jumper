﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endgameCapsule : MonoBehaviour
{
    public float offSet;
    public float speed = 0.2f;

    private Vector3 startPoint;
    private Vector3 endPoint;
    private bool direction = true;
    private Transform target;

    public void Start()
    {
        target = GetComponent<Transform>();
        startPoint = target.transform.position;
        endPoint = startPoint;
        endPoint.y = startPoint.y + offSet;
    }

    public void FixedUpdate()
    {
        /// makes the finished crystal move up and down
        if(direction == true)
        {
            float step = speed * Time.deltaTime;
            target.position = Vector3.MoveTowards(target.position, endPoint, step);
            if(target.position.y >= (endPoint.y))
            { direction = false; }
        }

        if (direction == false)
        {
            float step = speed * Time.deltaTime;
            target.position = Vector3.MoveTowards(target.position, startPoint, step);
            if (target.position.y <= startPoint.y)
            { direction = true; }
        }
    }
}
