﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnd : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        // will call the function when anything collides with the trigger capsule, (will only ever be the character)
        ScoreBoard.instance.FinishLevel();
    }
}
