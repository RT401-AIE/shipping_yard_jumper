﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using VisualFXSystem;

public class JumpingPads : MonoBehaviour
{
    Vector3 Velocity = new Vector3();
    public float jumpHeight = 10;

    public VisualFX fx;
    public Transform fxSpawnPoint;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            /// takes the inputed float and makes it into the force applyied to upwards velocity
            float force = Mathf.Sqrt(-(jumpHeight * 2 * Physics.gravity.y));


            CharacterMover otherController = other.GetComponent<CharacterMover>();

            Velocity.x = otherController.velocity.x;
            Velocity.z = otherController.velocity.z;
            Velocity.y = force;

            otherController.velocity = Velocity;

            // move to be directly under the player
            Vector3 spawnPos = fxSpawnPoint.position;
            spawnPos.x = other.transform.position.x;
            spawnPos.z = other.transform.position.z;
            fxSpawnPoint.transform.position = spawnPos;

            // play an explosion effect 
            if (fx != null)
                fx.Begin(fxSpawnPoint);
        }
    }
}
